import { shuffleArray, getRidOfEmtyInArray, arrayMove, arrayMoveForce, moveDown, moveUp } from '../app/helpers/helpers';

describe('Проверяем shuffle', () => {
  it('перетосовываем массив', () => {
    const arr = [1, 2, 3, 1, 18];
    expect(shuffleArray(arr)).not.toEqual(arr);
  });
});

describe('Проверяем удаление пустых массивов из массива', () => {
  it('[[1],[2],[]] => [1,2]', () => {
    const arr = [[1], [2], []];
    expect(getRidOfEmtyInArray(arr)).toEqual([[1], [2]]);
  });
  
  it('[[1],[2],[4],[]] => [[1],[2],[4]]', () => {
    const arr = [[1], [2], [4], []];
    expect(getRidOfEmtyInArray(arr)).toEqual([[1], [2], [4]]);
  });

  it('[[1],2,[4],[]] => [[1],[4]]', () => {
    const arr = [[1], 2, [4], []];
    expect(getRidOfEmtyInArray(arr)).toEqual([[1], [4]]);
  });

  it('[[1],[2],undefined,[]] => [[1],[2],[4]]', () => {
    const arr = [[1], [2], undefined, []];
    expect(getRidOfEmtyInArray(arr)).toEqual([[1], [2]]);
  });

  it('[[1],[2],undefined,[]] => [[1],[2],[4]]', () => {
    const arr = [[1], null, undefined, []];
    expect(getRidOfEmtyInArray(arr)).toEqual([[1]]);
  });

  it('[[1],[2],"undefined",[], ""] => [[1],[2],[4]]', () => {
    const arr = [[1], null, 'undefined', [], ''];
    expect(getRidOfEmtyInArray(arr)).toEqual([[1], 'undefined']);
  });
});

describe('Проверяем array move', () => {
  it('смещаем элемент в масиве', () => {
    const arr = [1, 2, 3, 1, 18];
    expect(arrayMove(arr, 2, 4)).toEqual([1, 2, 1, 18, 3]);
  });
  
  it('смещаем элемент в масиве', () => {
    const arr = [1, 2, 3, 1, 18, 33];
    expect(arrayMove(arr, 0, arr.length - 1)).toEqual([2, 3, 1, 18, 33, 1]);
  });
  
  it('смещаем элемент в масиве', () => {
    const arr = [1, 2, 3, 1, 18, 33];
    expect(arrayMove(arr, 0, -1)).toEqual([1, 2, 3, 1, 18, 33]);
  });
  
  it('смещаем элемент в масиве расширяя масив', () => {
    const arr = [1, 2, 3, 1, 18, 33];
    expect(arrayMoveForce(arr, 0, arr.length + 1)).toEqual([2, 3, 1, 18, 33, undefined, undefined, 1]);
  });

  it('смещаем элемент в масиве не расширяя масив', () => {
    const arr = [1, 2, 3, 1, 18, 33];
    expect(arrayMove(arr, 0, arr.length + 1)).toEqual([2, 3, 1, 18, 33, 1]);
  });
  
  it('смещаем элемент на один вверх', () => {
    const arr = [1, 2, 3, 1, 18, 33];
    expect(moveUp(arr, 0)).toEqual([2, 1, 3, 1, 18, 33]);
  });
  it('смещаем элемент на один вверх', () => {
    const arr = [1, 2, 3, 1, 18, 33];
    expect(moveUp(arr, arr.length - 1)).toEqual([1, 2, 3, 1, 18, 33]);
  });
  
  it('смещаем элемент на один вниз', () => {
    const arr = [1, 2, 3, 1, 18, 33];
    expect(moveDown(arr, 0)).toEqual([1, 2, 3, 1, 18, 33]);
  });
});
