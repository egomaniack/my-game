import * as React from 'react';
import { RulesDispatchToProps } from '../../containers/rules';
import { RulesWraper, BonusWrapper, BonusesWrapper, BonusText } from './styled';
import { icons } from '../../components/icons';
import { Bonus } from '../../model/question';

interface RulesProps extends RulesDispatchToProps {}

const steps: JSX.Element[] = [
    (
        <p>
            Команды по очереди открывают карточки на игровом поле,
            после чего отвечают на вопросы, соревнуясь на кнопке.
            Стоимость вопросов распределена рандомно. При правильном ответе
            на вопрос команда зарабатывает столько баллов, сколько указано
            на карточке, при неправильном эти баллы отнимаются.
        </p>
    ), (
        <p>
            Команда имеет право взять Джокера в любой момент
            игры при условии положительного баланса.
        </p>
    ), (
        <p>
            Некоторые карточки снабжены бонусами, тоже рандомно.
            Бонус действует для той команды, которая его открыла
        </p>
    ), (
        <BonusesWrapper>
            <BonusWrapper>
                {icons[Bonus.x2]}
                <BonusText>Стоимость вопроса удваивается</BonusText>
            </BonusWrapper>
            <BonusWrapper>
                {icons[Bonus.bomb]}
                <BonusText>Команда соперника не соревнуется в этом раунде на кнопке</BonusText>
            </BonusWrapper>
            <BonusWrapper>
                {icons[Bonus.hart]}
                <BonusText>При неправильном ответе очки не сгорают</BonusText>
            </BonusWrapper>
            <BonusWrapper>
                {icons[Bonus.poo]}
                <BonusText>При верном ответе у команды соперника отнимается стоимость карточки</BonusText>
            </BonusWrapper>
            <BonusWrapper>
                {icons[Bonus.boys]}
                {icons[Bonus.girls]}
                <BonusText>За столом остаются только мальчики/девочки</BonusText>
            </BonusWrapper>
        </BonusesWrapper>
    ),
];

interface RulesState {
    stepIndex: number;
    steps: any[];
}

class View extends React.PureComponent <RulesProps, RulesState> {
    state = {
        stepIndex: 0,
        steps
    };

    componentDidMount() {
        this.switchIndex(10000);
    }

    switchIndex = (milisec: number) => {
        setTimeout(() => {
            this.nextIndex();
            this.switchIndex(milisec);
        },
                   milisec
        );
    }

    nextIndex = () => {
        const { stepIndex } = this.state;
        const nextIndex = stepIndex === this.state.steps.length - 1 ? 0 : stepIndex + 1;
        this.setState({stepIndex: -1}, () => this.setState({stepIndex: nextIndex}));
    }

    render() {
        return (
            <RulesWraper onClick={this.props.toBack}>
                {steps[this.state.stepIndex]}
            </RulesWraper>
        );
    }
}

export default View;