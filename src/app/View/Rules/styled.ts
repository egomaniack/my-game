import styled, { keyframes } from 'styled-components';

import Colors from '../../constants/colors';

const iconSize = 150;

const reveal = keyframes`
  from {
    opacity: 0;
    transform: translateY(20px);
    transform: scale(0.82);
  }

  to {
    opacity: 1;
    transform: translateY(0);
    transform: scale(1);
  }
`;

const rotate = keyframes`
  0% {
    transform: rotateY(0deg);
  }

  50% {
    transform: rotateY(180deg);
  }

  100% {
    transform: rotateY(360deg);
  }
`;

export const RulesWraper = styled.div`
  color: ${Colors.white};
  height: 85vh;
  text-align: left;
  font-size: 3vh;
  font-family: 'Segoe UI', Tahoma, Geneva, Verdana, sans-serif;
  padding: 20px;
  display: flex;
  flex-direction: column;
  justify-content: center;

  & > div > div svg {
    min-width: ${iconSize}px;
    max-width: ${iconSize}px;
    min-height: ${iconSize}px;
    max-height: ${iconSize}px;
    animation: 8s ${rotate} linear infinite;
    transform-style: preserve-3d;
  }

  & div div:last-child {
    width: 88%;
  }

  p {
    margin-bottom: 0;
    text-align: center;
    animation: ${reveal} 2s cubic-bezier(0.46, 0.01, 0.57, 0.99);
  }
`;

export const BonusesWrapper = styled.div`
  display: flex;
  flex-wrap: wrap;
  margin-top: auto;
  animation: ${reveal} 2s cubic-bezier(0.46, 0.01, 0.57, 0.99);
`;

export const BonusWrapper = styled.div`
  width: 40%;
  padding: 10px 5%;
  display: flex;
  border-bottom: 1px solid rgba(255, 255, 255, 0.15);
`;

export const BonusText = styled.span`
  margin-left: 5%;
  display: flex;
  justify-content: center;
  flex-direction: column;
`;
