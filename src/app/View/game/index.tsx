import * as React from 'react';

import { PlayingCard } from '../../components/plaingCard';
import { GameCard } from '../../model/gameCard';
import { generateCards, CardsONScreen } from '../../helpers/generators';
import { GameStateToProps, GameDispatchToProps } from '../../containers/game';
import { getNotAnsweredQuestion } from '../../selectors/questions';
import { Question } from '../../model/question';
import { QuestionView } from '../../components/Question';
import { isEnoughQuestions } from '../../helpers/questions';
import { toRulesBindingKeys, backToSettingsBindingKeys } from '../../constants';
import { CardsBackCover } from '../../reducers/questions';

import { Wrapper, Desk, Letters, Numbers } from './styled';

interface ViewState {
  cards: GameCard[];
  currentQuestion: Question;
}

interface GameViewProps extends GameStateToProps, GameDispatchToProps {}

class View extends React.PureComponent <GameViewProps, ViewState> {

  constructor(props: GameViewProps) {
    super(props);

    this.state = {
      cards: generateCards(props.minQCount),
      currentQuestion: undefined,
    };
    window.addEventListener('keypress', this.handleHotKey);
  }

  componentWillUnmount() {
    window.removeEventListener('keypress', this.handleHotKey);
  }

  handleHotKey = (e: any) => {
    if (backToSettingsBindingKeys.includes(e.key)) {
      this.props.toSettings();
    } else if (toRulesBindingKeys.includes(e.key)) {
      const { cards }  = this.state;
      if (!this.chechClosedCards(cards)) {
        this.props.toRules(); // Сбрасывается все
      }
    }
  }

  chechClosedCards = (cards: GameCard[]): boolean => !!cards.some(c => !c.flipped);

  setCardQuestion = (cards: GameCard[], newCard: GameCard): GameCard[] =>
    cards.map(c => c.id === newCard.id && newCard || c)

  flipCard = (card: GameCard) => {
    let changingCard: GameCard = {...card, flipped: true};
    const { cards } = this.state;
    const updateCards = cards.map(c => c.id === card.id && changingCard || c);
    this.setState({cards: updateCards});
  }

  showQuestion = (card: GameCard) => {
    if (!!card.question) {
      this.setState({currentQuestion: card.question});
    } else {
      const q = getNotAnsweredQuestion(this.props.questions);
      this.setState({
        currentQuestion: q,
        cards: this.setCardQuestion(this.state.cards, {...card, question: q}),
      });
    }
  }

  handleCardClick = (card: GameCard) => {
    if (!card.flipped) {
      this.flipCard(card);
    } else {
      this.showQuestion(card);
    }
  }

  closeQuestion = () => {
    this.props.setAnswered(this.state.currentQuestion);
    this.setState({currentQuestion: undefined});

    const { cards }  = this.state;
    if (!this.chechClosedCards(cards)) {
      this.setState({cards: []});
      if (isEnoughQuestions(this.props.questions, this.props.minQCount)) {
        setTimeout(() => this.setState({cards: generateCards(this.props.minQCount)}), 0);
      } else {
        this.props.toSettings();
      }
    }
  }
  
  render() {
    const { cards, currentQuestion } = this.state;
    const { minQCount, cover, bgColor, noBonusMode } = this.props;
    const showSides = cover !== CardsBackCover.Other;

    return (
      <Wrapper color={bgColor} showSides={showSides}>
        {!!currentQuestion &&
          <QuestionView onClick={this.closeQuestion} question={currentQuestion} />}
          {showSides && [
            <Letters key="Letters" minQCount={minQCount}>
              <span>A</span>
              <span>B</span>
              <span>C</span>
              {minQCount === CardsONScreen.Twelve &&
                <span>D</span>}
            </Letters>,
            <Numbers key="Numbers">
              <span>1</span>
              <span>2</span>
              <span>3</span>
            </Numbers>
          ]}
        <Desk minQCount={minQCount} showSides={showSides}>
          {cards
            .map((card, key) => (
              <PlayingCard
                bgColor={bgColor}
                coverIndex={card.coverIndex}
                noBonusMode={noBonusMode}
                bgCover={cover}
                onAction={() => this.handleCardClick(card)}
                key={key}
                card={card}
              />
          ))}
        </Desk>
      </Wrapper>
    );
  }
}

export default View;
