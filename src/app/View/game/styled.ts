import styled from 'styled-components';
import { CardsONScreen } from '../../helpers/generators';

interface IHaveShowSides {
  showSides?: boolean;
}

interface IWrapperProps extends IHaveShowSides {
  color: string;
}

export const Wrapper = styled.div`
  width: 100vw;
  height: 100vh;
  background-color: ${(props: IWrapperProps) => props.color};
  display: grid;
  grid-template-columns: ${(props: IWrapperProps) => props.showSides ? '5v' : 0}w auto;
  grid-template-rows: ${(props: IWrapperProps) => props.showSides ? '7vh' : 0} auto;
  color: white;
  font-size: 10vh;
`;

interface DeskProps extends IHaveShowSides{
  minQCount: CardsONScreen;
}

export const Desk = styled.div`
  display: grid;
  grid-column: 2;
  grid-row: 2;
  grid-template-columns: ${(props: DeskProps) => props.minQCount === CardsONScreen.Twelve ? 'repeat(4, 23vw)' : 'repeat(3, 30vw)'};
  grid-template-rows: repeat(3, ${(props: IHaveShowSides) => props.showSides ? '30vh' : '33vh'});
`;

export const Letters = styled.div`
  grid-column: 2;
  display: grid;
  grid-template-columns: ${(props: DeskProps) => props.minQCount === CardsONScreen.Twelve ? 'repeat(4, 23vw)' : 'repeat(3, 30vw)'};
  font-family: 'Gill Sans', 'Gill Sans MT', Calibri, 'Trebuchet MS', sans-serif;

  & > span {
    font-size: 7vh;
    align-self: center;
    justify-self: center;
  }
`;

export const Numbers = styled.div`
  display: grid;
  grid-row: 2;
  grid-template-rows: repeat(3, 30vh);
  font-family: 'Gill Sans', 'Gill Sans MT', Calibri, 'Trebuchet MS', sans-serif;

  & > span {
    align-self: center;
    justify-self: center;
  }
`;
