import * as React from 'react';
import Button from '@material-ui/core/Button';
import Radio from '@material-ui/core/Radio';
import ListItem from '@material-ui/core/ListItem';
import IconButton from '@material-ui/core/IconButton';
import ListItemText from '@material-ui/core/ListItemText';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import RadioGroup from '@material-ui/core/RadioGroup';
import ArrowDropUp from '@material-ui/icons/ArrowDropUp';
import ArrowDropDown from '@material-ui/icons/ArrowDropDown';
import Edit from '@material-ui/icons/Edit';
import Delete from '@material-ui/icons/Delete';
import Switch from '@material-ui/core/Switch';

import { SettingsStateToProps, SettingsDispatchToProps } from '../../containers/settings';
import Modal from '../../components/modal';
import { QuestionDialog } from '../../components/QuestionDialog';
import {
  Wrapper,
  PlaySection,
  QuestionsSection,
  QuestionBody,
  OtherSettingsWrapper,
  QuestionsList,
  QuestionItem,
  SettingsBlock,
  SettingsBlockTitle,
  MyFormControll,
  InlineImage,
} from './styled';
import { Question } from '../../model/question';
import { isEnoughQuestions } from '../../helpers/questions';
import { CardsONScreen } from '../../helpers/generators';
import { CardsBackCover } from '../../reducers/questions';
import { bg } from '../../components/frontCardSide/bg';
import { toRulesBindingKeys } from '../../constants';
import { isProd } from '../../constants/api';
// import { get } from '../../helpers/api';
// import { Requests } from '../../constants/api';

export enum ModalMode {
  off,
  add = 'Добавить',
  edit = 'Изменить'
}

interface ViewProps extends SettingsStateToProps, SettingsDispatchToProps { }

interface ViewState {
  modalMode: ModalMode;
  editingQuestion: Question | null;
  editingRoundindex: number;
}

const getTitle = (q: Question): string => q.title || `${q.body.slice(0, 20)}...`;

const questionRender = (q: Question, index: number, up: () => void, down: () => void) => (
  <QuestionBody answered={q.answered}>
    {index}
    <IconButton onClick={up}>
      <ArrowDropUp />
    </IconButton>
    <IconButton onClick={down}>
      <ArrowDropDown />
    </IconButton>
    {getTitle(q)}
  </QuestionBody>
);
let sended: boolean = false;
class View extends React.PureComponent <ViewProps, ViewState> {

  constructor(props: ViewProps) {
    super(props);
    this.state = {
      modalMode: ModalMode.off,
      editingQuestion: null,
      editingRoundindex: 0
    };

    props.getquestions();
    props.init();

    // if (props.user === null) {
    //   console.info('need log in');
    // }
    if (!sended && isProd && !(window as any).accessGranded && !localStorage.getItem('access')) {
      const frase = prompt('Секретное слово');
      if (frase !== 'всемпохуй' && frase !== 'мартышлюшка') {
          alert('Для изменений необходимо знать секретное слово');
          this.props.toGame();
      } else {
        localStorage.setItem('access', String(performance.now()));
        (window as any).accessGranded = true;
      }
      // get({apiAdress: Requests.SECURITY, getParams: { security: frase }})(((() => {}) as any));
      sended = true;
    }
    if (!(window as any).accessGranded && isProd && !localStorage.getItem('access')) {
      this.props.toGame();
    }
  }

  componentWillUnmount () {
    window.removeEventListener('keypress', this.handleHotKey);
  }

  handleHotKey = (e: any) => {
    if (toRulesBindingKeys.includes(e.key)) {
      this.props.toRules();
    }
  }

  openAddQuestion = () => this.setState({modalMode: ModalMode.add});

  editQuestion = (q: Question) => {
    this.setState({
      editingQuestion: q,
      modalMode: ModalMode.edit
    });
  }

  moveUp = (index: number): void => {
    this.props.moveQUp(this.props.questions, index);
  }

  moveDown = (index: number): void => {
    this.props.moveQDwn(this.props.questions, index);
  }

  delAnswConfirmed = () => {
    confirm('Точно удалить?') && this.props.deleteAnswered();
  }

  handleClickChangeQValue = (e: any) => this.props.setQCount(Number(e.target.value));
  handleClickChangeCover = (e: any) => this.props.setCardCover(Number(e.target.value));

  handleBonusModeToggle = () => {
    this.props.toggleBonusMode();
  }

  handleColorPicked = (e: React.SyntheticEvent<HTMLInputElement>) => {
    this.props.setColor(e.currentTarget.value);
  }

  renderQuestionItem = (q: Question, j: number): JSX.Element => {
    const { delQuestion } = this.props;

    return (<QuestionItem key={q.id}>
      <ListItemText primary={questionRender(q, j + 1, () => this.moveUp(j), () => this.moveDown(j))} />
      {q.picture && <InlineImage src={q.picture}/>}
      <p>
        <IconButton onClick={() => this.editQuestion(q)}>
          <Edit />
        </IconButton>
        <IconButton onClick={() => confirm('Точно удалить?') && delQuestion(q)}>
          <Delete />
        </IconButton>
      </p>
    </QuestionItem>);
  }

  render() {
    const {
      toGame,
      addQuestion,
      changeQuestion,
      questions,
      minQCount,
      bgColor,
    } = this.props;

    const canIChange = !this.props.user;
    return (
      <Wrapper color={bgColor}>
        <QuestionsSection>
          <div>
            <Button disabled={!canIChange} variant="raised" color="primary" onClick={this.openAddQuestion}>Добавить вопрос</Button>
            <Button disabled={!canIChange} variant="raised" color="primary" onClick={this.props.toRules}>Правила</Button>
          </div>
          <QuestionsList>
            {questions.map(this.renderQuestionItem)}
          </QuestionsList>
          <ListItem
            className="del-button"
            dense={true} 
            button={true}
            disabled={!canIChange} 
            onClick={this.delAnswConfirmed}
          >
            <ListItemText primary="Удалить отвеченные" />
          </ListItem>
          <ListItem disabled={!canIChange} dense={true} button={true} onClick={this.props.resetToUnAnswered}>
            <ListItemText primary="Сбросить все на не отвечено" />
          </ListItem>
        </QuestionsSection>
        <OtherSettingsWrapper>
          <SettingsBlock>
            <SettingsBlockTitle>
              Выберите количество вопросов на раунд
            </SettingsBlockTitle>
            <RadioGroup value={`${this.props.minQCount}`} onChange={this.handleClickChangeQValue}>
              <FormControlLabel
                value={String(CardsONScreen.Twelve)}
                label="12 вопросов"
                control={<Radio color="primary" />}
              />
              <FormControlLabel
                value={String(CardsONScreen.Nine)}
                label="9 вопросов"
                control={<Radio color="primary" />}
              />
            </RadioGroup>
          </SettingsBlock>
          <SettingsBlock>
            <SettingsBlockTitle>
              Выберите рубашку карточек
            </SettingsBlockTitle>
            <RadioGroup value={`${this.props.cardsCover}`} onChange={this.handleClickChangeCover}>
              <MyFormControll
                value={String(CardsBackCover.BrainDo)}
                label={bg[CardsBackCover.BrainDo](null, '#fff')}
                control={<Radio color="primary" />}
              />
              <MyFormControll
                value={String(CardsBackCover.Lines)}
                label={bg[CardsBackCover.Lines]()}
                control={<Radio color="primary" />}
              />
              {/* <MyFormControll
                value={String(CardsBackCover.Alpha)}
                label={bg[CardsBackCover.Alpha]()}
                control={<Radio color="primary" />}
              /> */}
              {/* <MyFormControll
                value={String(CardsBackCover.Other)}
                label={bg[CardsBackCover.Other](0)}
                control={<Radio color="primary" />}
              /> */}
            </RadioGroup>
          </SettingsBlock>
          <SettingsBlock>
            <SettingsBlockTitle>
              Другое
            </SettingsBlockTitle>
              <FormControlLabel
                label={this.props.bonusMode ? 'Бонусы не нужны' : 'Бонусы нужны'}
                control={<Switch onChange={this.handleBonusModeToggle} checked={!this.props.bonusMode} />}
              />
              <FormControlLabel
                label="Цвет фона"
                control={<input type="color" defaultValue={bgColor} onChange={this.handleColorPicked}/>}
              />
          </SettingsBlock>
        </OtherSettingsWrapper>
        <PlaySection>
          {isEnoughQuestions(questions, minQCount)
              && <Button variant="raised" color="primary" onClick={toGame}>Играть</Button>}
        </PlaySection>

        {this.state.modalMode !== ModalMode.off && (
          <Modal close={() => this.setState({modalMode: ModalMode.off})}>
            <QuestionDialog
              change={(q: Question) => {
                changeQuestion(q);
                this.setState({modalMode: ModalMode.off});
              }}
              mode={this.state.modalMode}
              question={this.state.editingQuestion}
              index={this.props.questions.length + 1}
              cansel={() => this.setState({modalMode: ModalMode.off})}
              add={(q: Question) => {
                addQuestion(q);
                this.setState({modalMode: ModalMode.off});
              }}
            />
          </Modal>
        )}
      </Wrapper>
    );
  }
}

export default View;
