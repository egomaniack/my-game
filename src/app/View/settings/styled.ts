import styled from 'styled-components';
import FormControlLabel from '@material-ui/core/FormControlLabel';

import Colors from '../../constants/colors';

interface HaveColorProps {
  color: string;
}

export const Wrapper = styled.div`
  background-color: ${(props: HaveColorProps) => props.color || Colors.black};
  display: grid;
  grid-template-columns: 50vw 50vw;
  grid-template-rows: calc(100vh - 120px) 120px;
  grid-column-gap: 1vw;
  justify-items: center;
  align-items: center;

  & .del-button {
    margin-bottom: 12px;
  }
`;

export const MyFormControll = styled(FormControlLabel)`
  max-height: 200px;

  span:nth-child(2) {
    height: 100px;
    width: 100%;
  }
`;

export const QuestionsSection = styled.section`
  padding: 10px;
  display: flex;
  flex-direction: column;
  width: 80%;
  max-width: 600px;
  max-height: 100%;
  background-color: ${Colors.lightBG};
  box-shadow: 3px 3px 13px 13px rgba(0, 0, 0, 0.02);
  border-top: none;
  border-radius: 3px;
  overflow: auto;

  & > div {
    width: 100%;
    display: flex;
    justify-content: space-between;
    align-items: center;
    height: 64px;
  }

  ul {
    max-height: calc(100vh - 220px);
    overflow: auto;
  }
`;

export const PlaySection = styled.section`
  grid-column: 1/3;
  align-self: center;
`;

interface Answered {
  answered: boolean;
}

export const QuestionBody = styled.p `
  text-decoration: ${(props: Answered) => props.answered ? 'line-through' : 'none'};
  font-size: 18px;
`;

export const OtherSettingsWrapper = styled.div`
  background-color: ${Colors.white};
  grid-column: 2;
  align-self: center;
  width: 80%;
  padding: 12px;
  border-radius: 3px;
  max-height: 100%;
  overflow: auto;

  .cover {
    width: 100%;
    height: 100%;
    box-shadow: 1px 1px 2px 2px rgba(0, 0, 0, 0.07);
  }
`;

export const SettingsBlock = styled.div`
  margin: 15px;
  border: 1px solid ${Colors.mainColor};
  padding: 15px;
  padding-top: 5px;
  border-radius: 2px;
`;

export const SettingsBlockTitle = styled.h3`
  color: ${Colors.mainColor};
  font-family: Roboto;
  margin-top: 0;
  padding-top: 0;
`;

export const QuestionsList = styled.ul`
  list-style: none;
  padding-left: 0;
`;

export const QuestionItem = styled.li`
  display: flex;
  border-top: 0.5px solid ${Colors.mainColor};
  font-family: Roboto;
  align-items: center;
`;

interface InlineImageProps {
  src: string;
}

export const InlineImage = styled.div`
  background-image: url(${(props: InlineImageProps) => props.src});
  background-size: 100%;
  width: 50px;
  height: 50px;
`;
