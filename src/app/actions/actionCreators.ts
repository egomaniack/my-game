import * as ActionTypes from '../constants/actionTypes';
import { FlowAction, States } from '../reducers/flow';
import { Question, TQuestions } from '../model/question';
import { QuestionsAction, CardsBackCover } from '../reducers/questions';

export const deleteAnswered = (): QuestionsAction => ({
  type: ActionTypes.DELETE_ANSWERED,
});

export const toGame = (): FlowAction => ({
  type: ActionTypes.CHANGE_FLOW,
  payload: {
    stateName: States.Game
  }
});

export const toSettings = (): FlowAction => ({
  type: ActionTypes.CHANGE_FLOW,
  payload: {
    stateName: States.Settings
  }
});

export const toRules = (): FlowAction => ({
  type: ActionTypes.CHANGE_FLOW,
  payload: {
    stateName: States.Rules
  }
});

export const toBack = (): FlowAction => ({
  type: ActionTypes.POP_FLOW,
});

export const setQuestions = (questions: TQuestions): QuestionsAction => ({
	type: ActionTypes.SET_QUESTIONS,
	payload: {
		questions
	}
});

export const setQuestionsCount = (count: number): QuestionsAction => ({
  type: ActionTypes.SET_Q_COUNT,
  payload: {
    newCount: count
  }
});

export const addQuestion = (question: Question): QuestionsAction => ({
  type: ActionTypes.ADD_QUESTION,
  payload: { question }
});

export const changeQuestion = (question: Question): QuestionsAction => ({
  type: ActionTypes.CHANGE_QUESTION,
  payload: { question }
});

export const delQuestion = (question: Question): QuestionsAction => ({
  type: ActionTypes.DEL_QUESTION,
  payload: { question }
});

export const setAnswered = (question: Question): QuestionsAction => ({
  type: ActionTypes.SET_ANSWERED_Q,
  payload: { question }
});

export const setAllUnAnswered = (): QuestionsAction => ({
  type: ActionTypes.SET_ALL_QUESTIONS_UNANSWERED,
});

export const setFromLocal = (): QuestionsAction => ({
  type: ActionTypes.SET_Q_FROM_LOCAL,
});

export const setCardCover = (cover: CardsBackCover): QuestionsAction => ({
  type: ActionTypes.SET_COVER,
  payload: {
    newCover: cover
  }
});

export const toggleBonusMode = () => ({
  type: ActionTypes.TOGGLE_BONUS_MODE,
});

export function setColor(color: string) {
  return ({
    type: ActionTypes.SET_BG_COLOR,
    payload: {
      newColor: color,
    }
  });
}
