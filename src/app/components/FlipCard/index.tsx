import * as React from 'react';

import { Wrapper, Front, Back, Flipper } from './styled';

interface FLProps {
  front: JSX.Element;
  back: JSX.Element;
  flipped: boolean;
  onClick(): void;
}

const FlipCard = (props: FLProps): JSX.Element => {
  return (
    <Wrapper onClick={props.onClick}>
      <Flipper flipped={props.flipped}>
        <Front>
          {props.front}
        </Front>
        <Back>
          {props.back}
        </Back>
      </Flipper>
    </Wrapper>
  );
};

export default FlipCard;
