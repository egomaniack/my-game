import styled from 'styled-components';

export const Wrapper =  styled.div`
  perspective: 1000px;
  width: 100%;
  height: 100%;
  cursor: pointer;
`;

interface Flippable {
  flipped: boolean;
}

export const Flipper = styled.div`
  width: 100%;
  height: 100%;
  transition: 0.6s;
  transform-style: preserve-3d;
  position: relative;
  ${(props: Flippable) => props.flipped && 'transform: rotateY(180deg)'};
`;

export const Front = styled.div`
  width: 100%;
  height: 100%;
  z-index: 2;
  transform: rotateY(0deg);
  backface-visibility: hidden;
  position: absolute;
  top: 0;
  left: 0;
`;

export const Back = styled.div`
  width: 100%;
  height: 100%;
  transform: rotateY(180deg);
  backface-visibility: hidden;
  position: absolute;
  top: 0;
  left: 0;
`;
