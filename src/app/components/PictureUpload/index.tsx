import * as React from 'react';
let request = require('superagent');

import { Drop } from './styled';
import { PROTOCOL, SERVER_ADDR, SERVER_PORT, API_MAIN_PATH, Requests } from '../../constants/api';

interface PictureUploadProps {
    onUpload(url: string): void;
}

export class PictureUpload extends React.PureComponent <PictureUploadProps, any> {
    constructor(props: PictureUploadProps) {
        super(props);
        this.state = {
            filesToBeSent: null
        };
    }

    onDrop = (acceptedFiles: any[]) => {
        this.setState({filesToBeSent: acceptedFiles}, this.handleSendFile); 
    }

    handleSendFile = () => {
        if (this.state.filesToBeSent) {
            let filesArray = this.state.filesToBeSent;
            let req = request.post(`${PROTOCOL}://${SERVER_ADDR}:${SERVER_PORT}/${API_MAIN_PATH}/${Requests.PICTURE_UPLOAD}/`);

            req.attach(performance.now(), filesArray[0]);
            req.end((error: string, responce: { text: string }) => {

                if (error) {
                    console.error('send file error ocurred', error);
                }

                this.props.onUpload(`${PROTOCOL}://` + responce.text);
            });
        }
    }

    render() {

        return (
            <Drop multiple={false} onDrop={this.onDrop}>
                <div>Перетяни сюда картинку</div>
            </Drop>
        );
    }
}
