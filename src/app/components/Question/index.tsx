import * as React from 'react';
import { Question } from '../../model/question';
import { QuestionWrapper, Wrapper, BlackBG, QImage } from './styled';

interface QuestionViewProps {
    question: Question;
    onClick(): void;
}

export const QuestionView = (props: QuestionViewProps) => {
    const { question } = props;
    return (
        <Wrapper onClick={props.onClick}>
            {question.picture && <QImage picturePart={question.picturePart} src={question.picture}/>}
            <QuestionWrapper
                textSize={question.textSize}
                havePicture={!!question.picture}
                defaultValue={question.body}
                contentEditable={false}
                picturePart={question.picturePart}
            />
            <BlackBG />
        </Wrapper>
    );
};
