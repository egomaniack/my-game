import styled from 'styled-components';

export const Wrapper = styled.div`
  z-index: 10;
  position: fixed;
  top: 0;
  left: 0;
  width: 100vw;
  height: 100vh;
  text-align: center;
`;

interface QWrapperPorps extends IHavePicturePart {
  havePicture: boolean;
  textSize: number;
}

interface IHavePicturePart {
  picturePart: number;
}

export const QuestionWrapper = styled.textarea`
  width: 90vw;
  height: ${(props: QWrapperPorps) => props.havePicture ? `${100 - props.picturePart * 100}%` : '90vh'};
  text-align: left;
  font-size: ${(props: QWrapperPorps) => props.textSize}px;
  background: black;
  color: white;
  border: none;
  outline: 0;
  cursor: default;
`;

export const BlackBG = styled.div`
  top: 0;
  z-index: -1;
  position: absolute;
  background-color: black;
  width: 100%;
  height: 100%;
`;

export const QImage = styled.img`
  height: ${(props: IHavePicturePart) => props.picturePart * 100}%;
  max-width: 100%;
  display: block;
  margin: 0 auto;
`;
