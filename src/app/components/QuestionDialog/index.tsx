import * as React from 'react';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';


import { ModalMode } from '../../View/settings';
import { Question } from '../../model/question';
import { PictureUpload } from '../PictureUpload';

import { Wrapper, Range } from './styled';
import { QuestionView } from '../Question';

interface QuestionDialogProps {
  add(question: Question): void;
  change?(question: Question): void;
  cansel?(): void;
  mode: ModalMode;
  question?: Question;
  index: number;
}

interface QuestionDialogState extends Question {
  previewQuestion?: boolean;
}

export class QuestionDialog extends React.PureComponent <QuestionDialogProps, QuestionDialogState> {

  private titleInput: React.RefObject<HTMLInputElement> = React.createRef();

  constructor(props: QuestionDialogProps) {
    super(props);
    let { question, mode, index } = this.props;
    let startingTask: Question = mode === ModalMode.add && new Question({title: `Вопрос №${index}`}) || question;

    this.state = {
      ...startingTask,
    };
  }

  componentDidMount() {
    this.titleInput.current.focus();
    this.titleInput.current.select();
  }

  handleAdd = () => {
    const q = this.state;

    switch (this.props.mode) {
      case ModalMode.add:
        this.props.add(q);
        break;
      case ModalMode.edit:
        this.props.change(q);
        break;
      default:
        console.error('Modalmode = ' + this.props.mode);
        break;
    }
  }

  resetpicture = () => this.setState({ picture: undefined });

  handlePicturePartChange = (event: React.SyntheticEvent<HTMLInputElement>) => {
    const value = event.currentTarget.value;

    this.setState({ picturePart: Number(value) });
  }

  render() {
    let { mode, cansel } = this.props;
    let { title, body, textSize, picturePart } = this.state;
    return (
      <Wrapper>
        {this.state.picture && (
        <>
          <img src={this.state.picture}/>
          <span onClick={this.resetpicture}>x</span>
          <h5>Относительный размер картинки</h5>
          <Range
            onChange={this.handlePicturePartChange}
            type='range'
            value={picturePart || .45}
            min="0"
            max="1"
            step={.05}
          />
        </>)
      || <PictureUpload onUpload={(pictureUrl: string) => this.setState({ picture: pictureUrl })}/>}
        <h5>Размер шрифта в пикселях</h5>
        <TextField
          type="number"
          defaultValue={textSize}
          onChange={({target: { value }}) => this.setState({ textSize: Number(value) })}
        />
        <h5>Заголовок</h5>
        <TextField
          inputRef={this.titleInput}
          fullWidth={true}
          defaultValue={title}
          onChange={({target: { value }}) => this.setState({ title: value })}
        />
        <h5>Вопрос</h5>
        <TextField
          multiline={true}
          fullWidth={true}
          defaultValue={body}
          onChange={({target: { value }}) => this.setState({ body: value })}
        />
        <Button
          className="left"
          variant={'raised'}
          color={'primary'}
          onClick={this.handleAdd}>
          {mode}
        </Button>
        <Button
          className="right"
          variant={'raised'}
          color={'primary'}
          onClick={cansel}>
          Отмена
        </Button>
        <Button
          className="preview"
          onClick={() => this.setState({previewQuestion: true})}>
          preview
        </Button>
        {this.state.previewQuestion && (
          <QuestionView
            onClick={() => this.setState({previewQuestion: false})}
            question={this.state}
          />
        )}
      </Wrapper>
    );
  }
}
