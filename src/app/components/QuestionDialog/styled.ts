import styled from 'styled-components';

export const Wrapper = styled.div`
  padding: 15px;
  padding-top: 0;
  min-width: 300px;
  text-align: center;

  & > h5 {
    padding-top: 0;
  }

  & h5 {
    margin-bottom: 5px;
  }

  & button {
    margin-top: 15px;
  }

  button:last-child {
    float: right;
  }

  & > img {
    max-width: 300px;
  }

  & > span {
    position: absolute;
    margin-left: 10px;
    font-size: 20px;
    font-family: cursive;
    cursor: pointer;
  }

  .preview {
    font-size: 12px;
    margin-left: 12px;
  }

  .left {
    margin-right: 12px;
  }

  .right {
    margin-left: 12px;
  }
`;

export const Range = styled.input`
  -webkit-appearance: none;
  width: 100%;
  margin: 8.3px 0;

  &:focus {
    outline: none;
  }

  &::-webkit-slider-runnable-track {
    width: 100%;
    height: 3.4px;
    cursor: pointer;
    box-shadow: 0.9px 0.9px 2.7px #000000, 0px 0px 0.9px #0d0d0d;
    background: #b3ced6;
    border-radius: 1.3px;
    border: 0.2px solid rgba(1, 1, 1, 0);
  }
  &::-webkit-slider-thumb {
    box-shadow: 1.1px 1.1px 3.7px #000000, 0px 0px 1.1px #0d0d0d;
    border: 0px solid #000000;
    height: 20px;
    width: 20px;
    border-radius: 10px;
    background: #ffffff;
    cursor: pointer;
    -webkit-appearance: none;
    margin-top: -8.5px;
  }
  &:focus::-webkit-slider-runnable-track {
    background: #bad2da;
  }
  &::-moz-range-track {
    width: 100%;
    height: 3.4px;
    cursor: pointer;
    box-shadow: 0.9px 0.9px 2.7px #000000, 0px 0px 0.9px #0d0d0d;
    background: #b3ced6;
    border-radius: 1.3px;
    border: 0.2px solid rgba(1, 1, 1, 0);
  }
  &::-moz-range-thumb {
    box-shadow: 1.1px 1.1px 3.7px #000000, 0px 0px 1.1px #0d0d0d;
    border: 0px solid #000000;
    height: 20px;
    width: 20px;
    border-radius: 10px;
    background: #ffffff;
    cursor: pointer;
  }
  &::-ms-track {
    width: 100%;
    height: 3.4px;
    cursor: pointer;
    background: transparent;
    border-color: transparent;
    color: transparent;
  }
  &::-ms-fill-lower {
    background: #accad2;
    border: 0.2px solid rgba(1, 1, 1, 0);
    border-radius: 2.6px;
    box-shadow: 0.9px 0.9px 2.7px #000000, 0px 0px 0.9px #0d0d0d;
  }
  &::-ms-fill-upper {
    background: #b3ced6;
    border: 0.2px solid rgba(1, 1, 1, 0);
    border-radius: 2.6px;
    box-shadow: 0.9px 0.9px 2.7px #000000, 0px 0px 0.9px #0d0d0d;
  }
  &::-ms-thumb {
    box-shadow: 1.1px 1.1px 3.7px #000000, 0px 0px 1.1px #0d0d0d;
    border: 0px solid #000000;
    height: 20px;
    width: 20px;
    border-radius: 10px;
    background: #ffffff;
    cursor: pointer;
    height: 3.4px;
  }
  &:focus::-ms-fill-lower {
    background: #b3ced6;
  }
  &:focus::-ms-fill-upper {
    background: #bad2da;
  }
`;
