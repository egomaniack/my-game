import * as React from 'react';

import { Wrapper } from './styled';

interface BackSide {
  children: JSX.Element | string;
  icon: JSX.Element;
}

export const BackSide = (props: BackSide) => {
  return (
    <Wrapper>
      {props.icon}
      <span>{props.children}</span>
    </Wrapper>
  );
};
