import styled from 'styled-components';
import Colors from '../../constants/colors';

export const Wrapper = styled.div`
  text-align: center;
  position: relative;
  background-color: ${Colors.white};
  width: 100%;
  height: 100%;
  color: black;
  font-size: 9vw;
  font-weight: bold;
  font-family: 'Segoe UI', Tahoma, Geneva, Verdana, sans-serif;

  & > svg {
    position: absolute;
    bottom: 0;
    left: 20%;
    display: block;
    width: 60%;
    height: 60%;
  }
`;
