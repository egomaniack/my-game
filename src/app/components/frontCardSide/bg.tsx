import * as React from 'react';
import styled from 'styled-components';

import { CardsBackCover } from '../../reducers/questions';

interface IHasPicture {
	pic: string;
	bg?: string;
}

const Picture = styled.div`
  background: url(${(props: IHasPicture) => props.pic}) no-repeat center;
  background-color: ${(props: IHasPicture) => props.bg ? props.bg : 'black'};
  background-size: contain;
  width: 100%;
  height: 100%;
`;

const LinesPicture = styled.div`
  background: url(${(props: IHasPicture) => props.pic}) no-repeat center;
  background-color: white;
  background-size: contain;
  width: 100%;
  height: 100%;
`;

export const getPicture = (color: string): Array<JSX.Element> => [
	// <Picture pic={require('../../media/cards/people.jpg')} bg={color} />,
	// <Picture pic={require('../../media/cards/pic1.jpeg')} bg={color} />,
	// <Picture pic={require('../../media/cards/me.jpeg')} bg={color} />,
	// <Picture pic={require('../../media/cards/pic3.jpeg')} bg={color} />,
	// <Picture pic={require('../../media/cards/pic4.jpeg')} bg={color} />,
	// <Picture pic={require('../../media/cards/pic5.jpeg')} bg={color} />,
	// <Picture pic={require('../../media/cards/pic6.jpeg')} bg={color} />,
	// <Picture pic={require('../../media/cards/pic7.jpeg')} bg={color} />,
	// <Picture pic={require('../../media/cards/pic10.jpeg')} bg={color} />,
	// <Picture pic={require('../../media/cards/pic11.jpg')} bg={color} />,
	// <Picture pic={require('../../media/cards/pic12.jpeg')} bg={color} />,
	// <Picture pic={require('../../media/cards/pic13.jpeg')} bg={color} />,
	// <Picture pic={require('../../media/cards/pic14.jpeg')} bg={color} />,
	// <Picture pic={require('../../media/cards/pic15.jpeg')} bg={color} />,
	// <Picture pic={require('../../media/cards/pic16.jpeg')} bg={color} />,
	// <Picture pic={require('../../media/cards/pic17.jpg')} bg={color} />,
	// <Picture pic={require('../../media/cards/pic18.jpg')} bg={color} />,
	// <Picture pic={require('../../media/cards/pic19.jpeg')} bg={color} />,
	// <Picture pic={require('../../media/cards/pic20.jpeg')} bg={color} />,
];

interface BGs {
	[key: number]: (index?: number, color?: string) => JSX.Element | number;
}

// tslint:disable
export const bg: BGs = {
	[CardsBackCover.BrainDo]: (index: number = 0, color: string) => (
		<Picture pic={require('../../media/braindo.png')} bg={color} />
	),
	[CardsBackCover.Lines]: () => (
		<LinesPicture pic={require('../../media/lines.svg')} />
),
	[CardsBackCover.Alpha]: () => (
		<svg
			viewBox="0 0 200 200"
			className="cover">
			<g
				transform="translate(100, 20), rotate(180), scale(6)">
				<path
					style={{fill: '#ee2a23', fillOpacity: 1, fillRule: 'nonzero', stroke: 'none'}}
					d="m 0,0 c 1.779,0 2.338,-1.276 2.807,-2.55 0.197,-0.541 4.82,-13.201 4.959,-13.581 0.037,-0.106 0.012,-0.28 -0.151,-0.343 -0.164,-0.062 -2.566,-0.996 -2.699,-1.046 -0.133,-0.051 -0.282,0.023 -0.33,0.156 -0.05,0.131 -1.047,2.891 -1.159,3.199 l -6.852,0 c -0.108,-0.308 -1.117,-3.074 -1.162,-3.199 -0.046,-0.127 -0.189,-0.213 -0.331,-0.156 -0.142,0.056 -2.553,0.99 -2.697,1.046 -0.138,0.054 -0.201,0.211 -0.153,0.343 0.17,0.474 4.723,12.931 4.955,13.581 C -2.361,-1.295 -1.783,0 0,0 m -8.134,-22.653 c -0.144,0 -0.259,-0.12 -0.259,-0.266 l 0,-2.959 c 0,-0.146 0.115,-0.264 0.259,-0.264 l 16.261,0 c 0.147,0 0.262,0.118 0.262,0.264 l 0,2.959 c 0,0.146 -0.115,0.266 -0.262,0.266 l -16.261,0 m 8.118,18.519 -2.316,-6.955 4.67,0 -2.324,6.955 -0.03,0 z"
				/>
			</g>
		</svg>
	),
	[CardsBackCover.Other]: (index: number, color: string) => {
		return getPicture(color)[index];
	}
};
