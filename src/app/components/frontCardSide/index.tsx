import * as React from 'react';

import { Wrapper } from './styled';
import { bg } from './bg';
import { CardsBackCover } from '../../reducers/questions';
import { getContrastYIQ } from '../../helpers/color';

interface BackSide {
  children?: JSX.Element | string;
  bgCover: CardsBackCover;
  coverIndex: number;
  bgColor: string;
}

class FrontSide extends React.Component <BackSide>{
  render() {
    return (
      <Wrapper>
        {bg[this.props.bgCover](this.props.coverIndex, getContrastYIQ(this.props.bgColor))}
      </Wrapper>
    );
  }
};

export { FrontSide }
