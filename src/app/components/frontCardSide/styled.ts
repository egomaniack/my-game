import styled from 'styled-components';
import Colors from '../../constants/colors';

export const Wrapper = styled.div`
  background-color: ${Colors.white};
  width: 100%;
  height: 100%;
  text-align: center;
  font-size: 10vw;
  font-family: 'Segoe UI', Tahoma, Geneva, Verdana, sans-serif;
  display: flex;
  align-items: center;
  justify-content: center;

  & > svg {
    width: 100%;
    height: 100%;
  }
`;
