import * as React from 'react';
import { Wrapper, Content } from './styled';

interface ModalProps {
  children: any;
  close(): void;
}

class Modal extends React.Component<ModalProps> {

  handleWrapperClick = (e: any) => {
    if (e.target.id === 'modal-wrapper') {
      this.props.close();
    }
  };

  render() {
    const { children, close} = this.props;

    return (
      <Wrapper onClick={this.handleWrapperClick} id="modal-wrapper">
        <Content>
          {children}
          <span onClick={close} className="close">&times;</span>
        </Content>
      </Wrapper>
    );
  }
};

export default Modal;
