import * as React from 'react';

import FlipCard from '../FlipCard';
import { icons } from '../icons';
import { BackSide } from '../backCardSide';
import { FrontSide } from '../frontCardSide';
import { GameCard } from '../../model/gameCard';
import { Wrapper } from './styled';
import { CardsBackCover } from '../../reducers/questions';

interface PCardProps {
  card: GameCard;
  bgCover: CardsBackCover;
  onAction(): void;
  noBonusMode: boolean;
  coverIndex: number;
  bgColor: string;
}

export const PlayingCard = (props: PCardProps): JSX.Element => {
  return (
    <Wrapper>
      <FlipCard
        onClick={props.onAction}
        front={<FrontSide bgColor={props.bgColor} coverIndex={props.coverIndex} bgCover={props.bgCover}/>}
        back={<BackSide icon={!props.noBonusMode && icons[props.card.bonus]}>{(props.card.cost * 100).toString()}</BackSide>}
        flipped={props.card.flipped}
      />
    </Wrapper>
  );
};
