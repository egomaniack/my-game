export const LOADING = 'LOADING';
export const LOADED = 'LOADED';
export const CHANGE_FLOW = 'CHANGE_FLOW';
export const POP_FLOW = 'POP_FLOW';
export const ADD_TEAM = 'ADD_TEAM';
export const DEL_TEAM = 'DEL_TEAM';
export const RESET_TEAM = 'RESET_TEAM';
export const ADD_QUESTION = 'ADD_QUESTION';
export const DELETE_ANSWERED = 'DELETE_ANSWERED';
export const DEL_QUESTION = 'DEL_QUESTION';
export const CHANGE_QUESTION = 'CHANGE_QUESTION';

export const SET_QUESTIONS = 'SET_QUESTIONS';
export const SET_ANSWERED_Q = 'SET_ANSWERED_Q';
export const SET_TEAMS = 'SET_TEAMS';
export const SET_ALL_QUESTIONS_UNANSWERED = 'SET_ALL_QUESTIONS_UNANSWERED;';
export const SET_Q_FROM_LOCAL = 'SET_Q_FROM_LOCAL;';
export const SET_Q_COUNT = 'SET_Q_COUNT';
export const SET_COVER = 'SET_COVER';

// Включает/выключает показ богнусов
export const TOGGLE_BONUS_MODE = 'TOGGLE_BONUS_MODE';

// Устанавливает цвет фона для игрового экрана.
export const SET_BG_COLOR = 'SET_BG_COLOR';

/** user action types */
export const SET_USER = 'SET_USER';
