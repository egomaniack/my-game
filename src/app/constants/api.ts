export const isProd = !process.env.itIsDev;

// Вклюаем при работе напрямую с silentgon.khabtech.site
const workWithSilentgon = true;

export const SERVER_ADDR: string = isProd ? 'silentgon.khabtech.site' : (workWithSilentgon ? 'silentgon.khabtech.site' : 'localhost');
export const SERVER_PORT: number = isProd ? 80 : (workWithSilentgon ? 80 : 8088);
export const PROTOCOL = 'http';

export const API_MAIN_PATH: string = 'v1';

export enum Requests {
	INIT = 'init',
	GET_TEAMS = 'get_teams',
	GET_QUESTIONS = 'get_questions',
	SEND_NEW_QUESTION = 'new_question',
	PICTURE_UPLOAD = 'pictureUpload',
	CHANGE_QUESTION = 'change_question',
	DELETE_QUESTION = 'delete_question',
	DELETE_ANSWERED = 'delete_answered',
	SET_ANSWERED_QUESTION = 'set_answered_question',
	RESET_TO_UNANSWERED = 'reset_to_unanswered',
	SECURITY = 'security',
}
