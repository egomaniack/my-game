export default {
  mainColor: '#606FC4',
  white: 'white',
  black: '#020202',
  lightBG: '#F9FAFF',
};