import { connect } from 'react-redux';
import { Dispatch } from 'redux';

import GameView from '../View/game';
import { send } from '../helpers/api';
import { setAnswered, toSettings, toRules } from '../actions/actionCreators';
import { TheState } from '../reducers';
import { Question, TQuestions } from '../model/question';
import { Requests } from '../constants/api';
import { CardsONScreen } from '../helpers/generators';
import { CardsBackCover } from '../reducers/questions';

export interface GameStateToProps {
    questions: TQuestions;
    minQCount: CardsONScreen;
    cover: CardsBackCover;
    noBonusMode: boolean;
    bgColor: string;

}

export interface GameDispatchToProps {
    setAnswered(q: Question): void;
    toSettings(): void;
    toRules(): void;
}

const mapStateToProps = (state: TheState): GameStateToProps => ({
    questions: state.questionsState.questions,
    minQCount: state.questionsState.roundQuestionsCount,
    cover: state.questionsState.cardsCover,
    bgColor: state.questionsState.bgColor,
    noBonusMode: state.questionsState.noBonusMode,
});

const mapDispatchtoProps = (dispatch: Dispatch<any>): GameDispatchToProps => ({
    setAnswered: (question: Question) => {
        send({ apiAdress: Requests.SET_ANSWERED_QUESTION, data: { question } })(dispatch);
        dispatch(setAnswered(question));
    },
    toSettings: () => dispatch(toSettings()),
    toRules: () => dispatch(toRules()),
});

export default connect(mapStateToProps, mapDispatchtoProps)(GameView);
