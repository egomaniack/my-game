import { connect } from 'react-redux';
import { Dispatch } from 'redux';

import RulesView from '../View/Rules';
import {
    toBack
} from '../actions/actionCreators';

export interface RulesDispatchToProps {
  toBack(): void;
}

const mapDispatchtoProps = (dispatch: Dispatch<any>): RulesDispatchToProps => ({
  toBack: () => dispatch(toBack()),
});

export default connect(null, mapDispatchtoProps)(RulesView);
