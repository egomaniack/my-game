import { connect } from 'react-redux';
import { Dispatch } from 'redux';

import SettingsView from '../View/settings';
import { get, send } from '../helpers/api';
import {
  toGame,
  setQuestions,
  deleteAnswered,
  addQuestion,
  delQuestion,
  changeQuestion,
  setAllUnAnswered,
  setFromLocal,
  setQuestionsCount,
  setCardCover,
  toRules,
  toggleBonusMode,
  setColor,
} from '../actions/actionCreators';
import { TheState } from '../reducers';
import { Question, TQuestions } from '../model/question';
import { Requests } from '../constants/api';
import { moveUp, moveDown } from '../helpers/helpers';
import { CardsONScreen } from '../helpers/generators';
import { CardsBackCover } from '../reducers/questions';
import { User } from '../model/user';

export interface SettingsStateToProps {
  questions: TQuestions;
  minQCount: CardsONScreen;
  cardsCover: CardsBackCover;
  user: User;
  bonusMode: boolean;
  bgColor: string;
}

export interface SettingsDispatchToProps {
  deleteAnswered(): void;
  delQuestion(q: Question): void;
	toGame(): void;
	getquestions(): void;
	addQuestion(q: Question): void;
  changeQuestion(q: Question): void;
  resetToUnAnswered(): void;
  moveQUp(array: TQuestions, index: number): void;
  moveQDwn(array: TQuestions, index: number): void;
  setQCount(count: CardsONScreen): void;
  setCardCover(cover: CardsBackCover): void;
  toRules(): void;
  init(): void;
  toggleBonusMode(): void;
  setColor(color: string): void;
}

const mapStateToProps = (state: TheState): SettingsStateToProps => ({
  questions: state.questionsState.questions,
  minQCount: state.questionsState.roundQuestionsCount,
  cardsCover: state.questionsState.cardsCover,
  bonusMode: state.questionsState.noBonusMode,
  bgColor: state.questionsState.bgColor,
  user: state.userState.user,
});

const mapDispatchtoProps = (dispatch: Dispatch<any>): SettingsDispatchToProps => ({
  deleteAnswered: () => 
    send({apiAdress: Requests.DELETE_ANSWERED}, deleteAnswered)(dispatch),
	toGame: () => dispatch(toGame()),
	toRules: () => dispatch(toRules()),
	getquestions: () => get({apiAdress: Requests.GET_QUESTIONS}, setQuestions, setFromLocal)(dispatch),
  addQuestion: (question: Question) =>
    send({apiAdress: Requests.SEND_NEW_QUESTION, data: { question } }, addQuestion)(dispatch),
  changeQuestion: (question: Question) => 
    send({apiAdress: Requests.CHANGE_QUESTION, data: { question } }, changeQuestion)(dispatch),
  delQuestion: (question: Question) => 
    send({apiAdress: Requests.DELETE_QUESTION, data: { question } }, delQuestion)(dispatch),
  resetToUnAnswered: () => {
    send({apiAdress: Requests.RESET_TO_UNANSWERED})(dispatch);
    dispatch(setAllUnAnswered());
  },
  init: () => {
    send({apiAdress: Requests.INIT})(dispatch);
    // dispatch(setAllUnAnswered());
  },
  setQCount: (count: CardsONScreen) => dispatch(setQuestionsCount(count)),
  moveQUp: (array: TQuestions, index: number) => dispatch(setQuestions(moveDown(array, index))),
  moveQDwn: (array: TQuestions, index: number) => dispatch(setQuestions(moveUp(array, index))),
  setCardCover: (cover: CardsBackCover) => dispatch(setCardCover(cover)),
  toggleBonusMode: () => dispatch(toggleBonusMode()),
  setColor: (color: string) => dispatch(setColor(color)),
});

export default connect(mapStateToProps, mapDispatchtoProps)(SettingsView);
