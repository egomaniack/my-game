import { Dispatch } from 'redux';
import qs from 'querystring';

import { SERVER_ADDR, SERVER_PORT, PROTOCOL, API_MAIN_PATH, Requests } from '../constants/api';
import { LOADING, LOADED } from '../constants/actionTypes';

interface SendParams {
  apiAdress: Requests;
  getParams?: object;
}
interface SendPostParams {
	apiAdress: Requests;
	data?: any;
}

export const get = (
	params: SendParams,
	actionCreator?: Function,
	errorAC: Function = actionCreator
) => (dispatch: Dispatch<any>) => {
	dispatch({type: LOADING});

	const gotAnswer = (an: any) => {
		!!actionCreator && dispatch(actionCreator(an));
		dispatch({type: LOADED});
	};
	
	const gotError = (an: any) => {
		!!errorAC && dispatch(errorAC());
		dispatch({ type: LOADED });
	};

	return fetch(`${PROTOCOL}://${SERVER_ADDR}:${SERVER_PORT}/${API_MAIN_PATH}/${params.apiAdress}/?${qs.stringify(params.getParams)}`)
		.then(answer => answer.json())
		.then(gotAnswer)
		.catch(gotError);
};

export const send = (params: SendPostParams, actionCreator?: Function) => (dispatch: Dispatch<any>) => {
	const myHeaders = new Headers();
	let formData = new FormData();
	formData.append('data', JSON.stringify(params.data));

	const myInit: RequestInit = {
		method: 'POST',
		headers: myHeaders,
		mode: 'cors',
		cache: 'default',
		body: formData,
	};
	
	const gotAnswer = (an: any) => {
		!!actionCreator && dispatch(actionCreator(an));
	};

	return fetch(`${PROTOCOL}://${SERVER_ADDR}:${SERVER_PORT}/${API_MAIN_PATH}/${params.apiAdress}/`, myInit)
		.then(answer => answer.json())
		.then(gotAnswer);
};
