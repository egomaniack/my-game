export const lightenDarkenColor = (col: string, amt: number): string => {
  if (col[0] !== 'r') {
    let usePound = false;
  
    if (col[0] === '#') {
        col = col.slice(1);
        usePound = true;
    }
  
    let num = parseInt(col, 16);
  
    // tslint:disable-next-line:no-bitwise
    let r = (num >> 16) + amt;
  
    if (r > 255) {
     r = 255;
    } else if (r < 0) {
      r = 0;
    }
  
    // tslint:disable-next-line:no-bitwise
    let b = ((num >> 8) & 0x00FF) + amt;
  
    if (b > 255) {
      b = 255;
    } else if (b < 0) {
      b = 0;
    }
  
    // tslint:disable-next-line:no-bitwise
    let g = (num & 0x0000FF) + amt;
  
    if (g > 255) {
      g = 255;
    } else if (g < 0) {
      g = 0;
    }
  
    // tslint:disable-next-line:no-bitwise
    return (usePound ? '#' : '') + (g | (b << 8) | (r << 16)).toString(16);
  } else {
    return col;
  }
};

export function getContrastYIQ(hexcolor: string): string {
  const r = parseInt(hexcolor.substr(1,2), 16);
  const g = parseInt(hexcolor.substr(3,2), 16);
  const b = parseInt(hexcolor.substr(5,2), 16);

  const yiq = ((r * 299) + (g * 587) + (b * 114)) / 1000;

  return (yiq >= 128) ? 'black' : 'white';
}

export function invertColor(hex: string) {
  if (hex.indexOf('#') === 0) {
      hex = hex.slice(1);
  }
  // convert 3-digit hex to 6-digits.
  if (hex.length === 3) {
      hex = hex[0] + hex[0] + hex[1] + hex[1] + hex[2] + hex[2];
  }
  if (hex.length !== 6) {
      throw new Error('Invalid HEX color.');
  }
  // invert color components
  var r = (255 - parseInt(hex.slice(0, 2), 16)).toString(16),
      g = (255 - parseInt(hex.slice(2, 4), 16)).toString(16),
      b = (255 - parseInt(hex.slice(4, 6), 16)).toString(16);
  // pad each with zeros and return
  return '#' + padZero(r) + padZero(g) + padZero(b);
}

function padZero(str: string, len: number = 2) {
  var zeros = new Array(len).join('0');
  return (zeros + str).slice(-len);
}
