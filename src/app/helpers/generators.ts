import { Bonus, GameCard } from '../model/gameCard';
import { shuffleArray } from './helpers';
import { randomInteger } from './math';
import { getPicture } from '../components/frontCardSide/bg';

export enum CardsONScreen {
  Nine = 9,
  Twelve = 12
}

interface Bonuses {
  [x: number]: Bonus[];
}

let nums: number[] = [];

const getNumber = (count: CardsONScreen, index: number) => {
  const maxPics = getPicture('').length-1;
  const freeIndexesCount = maxPics - nums.length;

  if(freeIndexesCount + index < count) {
    nums = [];
  }

  let nextIndex = randomInteger(1, maxPics);
  let i = 0;
  while (nums.includes(nextIndex)) {
    i++;
    nextIndex = randomInteger(1, maxPics);

    if (i === 1000) {
      nums = [];
    }
  }
  nums.push(nextIndex);

  return nextIndex;
}

export const generateCards = (count: CardsONScreen): GameCard[] => {

  let bonuses: Bonuses = {
    [CardsONScreen.Twelve]: shuffleArray([
      Bonus.bomb, Bonus.bomb,
      Bonus.poo, Bonus.poo,
      Bonus.hart, Bonus.hart,
      Bonus.x2, Bonus.x2,
      Bonus.girls, Bonus.boys,
    ]),
    [CardsONScreen.Nine]: shuffleArray([
      Bonus.bomb, Bonus.poo,
      Bonus.hart, Bonus.x2,
      Bonus.girls, Bonus.boys,
      Bonus.none, Bonus.none,
    ]),
  };

  interface Costs {
    [x: number]: number[];
  }

  let costs: Costs = {
    [CardsONScreen.Twelve]: shuffleArray([
      2, 2, 1, 1, 1,
      1, 1, 1, 1, 1,
      0.5, 0.5
    ]),
    [CardsONScreen.Nine]: shuffleArray([
      0.5, 0.5, 1, 1,
      1, 1, 2,
      0.5, 0.5
    ]),
  };

  let cards: GameCard[] = [];

  for (let i = 0; i < count; i++) {
    const cost = costs[count].pop();
    cards
      .push(new GameCard({
        bonus: cost !== 2 && bonuses[count].pop() || Bonus.none,
        id: performance.now() + i,
        cost,
        coverIndex: getNumber(count, i),
      }));
  }

  return cards;
};
