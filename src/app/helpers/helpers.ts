export const shuffleArray = (arr: Array<any>) => arr
  .map(a => [Math.random(), a])
  .sort((a, b) => a[0] - b[0])
  .map(a => a[1]);

export const getRidOfEmtyInArray = (arr: Array<any>): Array<any> => {
  let result: Array<any> = [];
  arr.forEach(el => el && (el.length > 0) && result.push(el));

  return result;
};

export const arrayMoveForce = (arr: Array<any>, oldIndex: number, newIndex: number) => {
  if (newIndex >= arr.length) {
    var k = newIndex - arr.length + 1;
    while (k--) {
      arr.push(undefined);
    }
  }
  arr.splice(newIndex, 0, arr.splice(oldIndex, 1)[0]);

  return arr;
};

export function arrayMove<T>(arr: Array<T>, oldIndex: number, newIndex: number): Array<T> {
  arr.splice(Math.max(0, newIndex), 0, arr.splice(oldIndex, 1)[0]);
  return arr;
}

export interface MoveInArray<T> {
  (arr: Array<T>, index: number): Array<T>;
}

export function moveUp<T> (arr: Array<T>, index: number): Array<T> {
  return arrayMove(arr, index, index + 1);
}

export function moveDown<T> (arr: Array<T>, index: number): Array<T> {
  return arrayMove(arr, index, index - 1);
}
