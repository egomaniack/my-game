import { TQuestions } from '../model/question';

export const isEnoughQuestions = (questions: TQuestions, minCount: number = 12) => 
    [...questions].filter(q => !q.answered).length >= minCount;
