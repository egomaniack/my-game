import * as React from 'react';
import { connect } from 'react-redux';
import LinearProgress from '@material-ui/core/LinearProgress';

import Views from './processes';
import { FlowState } from './reducers/flow';
import { TheState } from './reducers';

interface AppProps {
  flow: FlowState;
  loading: boolean;
}

class App extends React.PureComponent <AppProps, any> {
  render() {
    let Process = Views[this.props.flow.stateName];
    let { loading } = this.props;
    return (
      <>
        {loading && <LinearProgress />}
        <Process />
      </>
    );
  }
}

const mapStateToProps = (state: TheState): AppProps => ({
  flow: state.flow,
  loading: state.process.fetching,
});

const connectedApp = connect(mapStateToProps)(App);

export default connectedApp;