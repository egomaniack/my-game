import { randomInteger } from '../helpers/math';
import { Question } from './question';

export enum Bonus {
  none,
  bomb,
  poo,
  hart,
  x2,
  girls,
  boys,
}

interface GameCardConstructorProps {
  bonus?: Bonus;
  cost?: number;
  id?: number;
  coverIndex?: number;
}

export class GameCard {
  coverIndex: number;
  cost: number;
  flipped: boolean;
  bonus: Bonus;
  id: number;
  question: Question;

  constructor(props: GameCardConstructorProps) {
    this.bonus = props.bonus || Bonus.none;
    this.cost = props.cost || randomInteger(1, 9);
    this.coverIndex = props.coverIndex;
    this.flipped = false;
    this.id = props.id || performance.now();
  }
}
