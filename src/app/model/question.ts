import { defaultQTextSize } from '../constants/index';
export enum Bonus {
  none,
  bomb,
  poo,
  hart,
  x2,
  girls,
  boys,
}

interface QuestionConstructorProps {
  id?: number;
  title?: string;
  cost?: number;
  body?: string;
  bonus?: Bonus;
  picture?: string;
  textSize?: number;
  picturePart?: number;
}

export class Question {
  textSize: number;
  id: number;
  title: string;
  body: string;
  answered: boolean;
  picture: string;
  picturePart: number;

  constructor (props: QuestionConstructorProps) {
    this.textSize = props.textSize || defaultQTextSize;
    this.title = props.title || '';
    this.body = props.body || '';
    this.answered = false;
    this.id = props.id || performance.now();
    this.picture = props.picture;
    this.picturePart = props.picturePart || .45;
  }
}

export type TQuestions = Array<Question>;
