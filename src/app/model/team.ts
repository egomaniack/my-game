interface TeamConstructorProps {
  name: string;
}

export class Team {
  private _name: string;
  private _points: number;
  private _id: number;

  constructor({ name }: TeamConstructorProps) {
    this._name = name;
    this._points = 0;
    this._id = performance.now();
  }

  get id(): number {
    return this._id;
  }

  get name(): string {
    return this._name;
  }

  get points(): number {
    return this._points;
  }

  changePoints(diff: number) {
    this._points += diff;
  }
}
