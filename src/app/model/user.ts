
/**
 * Объект пользователя.
 *
 * @param {string} name Имя.
 */
export interface User {
  name: string;
}
