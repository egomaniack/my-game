import GameView from '../containers/game';
import SettingsView from '../containers/settings';
import RulesView from '../containers/rules';
import { States } from '../reducers/flow';

export default {
  [States.Game]: GameView,
  [States.Settings]: SettingsView,
  [States.Rules]: RulesView,
};
