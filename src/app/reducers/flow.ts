import * as ActionTypes from '../constants/actionTypes';
import { HasType } from '../model/other';

export enum States {
  Settings,
  Game,
  Rules,
}

export interface FlowState {
  stateName: States;
  history: States[];
}

const initialState: FlowState = {
  stateName: States.Settings,
  history: [],
};

interface FlowPayload {
  stateName?: States;
}

export interface FlowAction extends HasType {
  payload?: FlowPayload;
}

export const flow = (state: FlowState = initialState, action: FlowAction): FlowState => {
  switch (action.type) {
    case ActionTypes.CHANGE_FLOW:
     return {
       history: [...state.history, state.stateName],
       stateName: action.payload.stateName
      };
    case ActionTypes.POP_FLOW:
      const poppedHistory: States[] = state.history;
      const prevStateName: States = poppedHistory.pop();
      return {
        history: poppedHistory,
        stateName: prevStateName,
      };
    default:
      return state;
  }
};
