import { Question } from '../model/question';

export const changeQuestion =
    (q: Question, questions: Array<Question>): Array<Question> => [...questions].map(qu => qu.id === q.id && q || qu);

export const deletQuestion =
    (q: Question, questions: Array<Question>): Array<Question> => [...questions].filter(qu => qu.id !== q.id);
    
export const deleteAnswered =
    (questions: Array<Question>): Array<Question> => [...questions].filter(qu => !qu.answered);

export const setAnswered =
    (q: Question, questions: Array<Question>): Array<Question> =>
        [...questions].map(qu => qu.id === q.id && {...q, answered: true} || qu);

export const setAllUnAnswered =
    (questions: Array<Question>): Array<Question> =>
        [...questions].map(q => ({...q, answered: false}));
