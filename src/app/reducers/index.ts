import { combineReducers } from 'redux';

import { flow, FlowState } from './flow';
import { questions, QuestionsState } from './questions';
import { ProcessState, process } from './process';
import { userReducer as userState, UserState } from './user';

export interface TheState {
  flow: FlowState;
  questionsState: QuestionsState;
  process: ProcessState;
  userState: UserState;
}

export default combineReducers({
  flow,
  process,
  questionsState: questions,
  userState,
});
