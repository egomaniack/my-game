import { HasEType } from '../model/other';
import { LOADING, LOADED } from '../constants/actionTypes';

export interface ProcessState {
	fetching: boolean;
}

const initialState: ProcessState = {
	fetching: false,
};

export interface ProcessAction extends HasEType {}

export const process = (state: ProcessState = initialState, action: ProcessAction): ProcessState => {
	switch (action.type) {
		case LOADING:
			return {
				fetching: true,
			};
		case LOADED:
			return {
				fetching: false,
			};
		default:
			return state;
	}
};
