import {
  SET_QUESTIONS,
  SET_Q_FROM_LOCAL,
  ADD_QUESTION,
  CHANGE_QUESTION,
  DEL_QUESTION,
  DELETE_ANSWERED,
  SET_ANSWERED_Q,
  SET_ALL_QUESTIONS_UNANSWERED,
  SET_Q_COUNT,
  SET_COVER,
  TOGGLE_BONUS_MODE,
  SET_BG_COLOR,
} from '../constants/actionTypes';
import { Question } from '../model/question';
import { HasEType } from '../model/other';
import { changeQuestion, deletQuestion, deleteAnswered, setAnswered, setAllUnAnswered } from './helpers';

export enum CardsBackCover {
  BrainDo,
  Lines,
  Alpha,
  Other,
}

export interface QuestionsState {
  questions: Array<Question>;
  roundQuestionsCount: number;
  cardsCover: CardsBackCover;
  noBonusMode: boolean;
  bgColor: string;
}

const initialState: QuestionsState = {
  questions: [],
  roundQuestionsCount: 12,
  cardsCover: CardsBackCover.BrainDo,
  noBonusMode: false,
  bgColor: 'black',
};

interface QPayload {
  questions?: Array<Question>;
  question?: Question;
  newCount?: number;
  newCover?: CardsBackCover;
  newColor?: string;
}

export interface QuestionsAction extends HasEType {
  payload?: QPayload;
}

const saveQuestions = (q: Array<Question>) => localStorage.setItem('questions', JSON.stringify(q));

export const questions = (state: QuestionsState = initialState, action: QuestionsAction): QuestionsState => {
  switch (action.type) {
    case SET_QUESTIONS:
      saveQuestions(action.payload.questions);
      return {
        ...state,
        questions: ([...action.payload.questions]),
      };
      case SET_Q_FROM_LOCAL:
      const qs: Array<Question> = JSON.parse(localStorage.getItem('questions'));
      return {
        ...state,
        questions: ([...(!!qs && qs || [])]),
      };
      case ADD_QUESTION:
      const qss = [...state.questions, action.payload.question];
      saveQuestions(qss);
      return {
        ...state,
        questions: qss,
      };
    case CHANGE_QUESTION:
      const chqss = changeQuestion(action.payload.question, state.questions);
      saveQuestions(chqss);
      return {
        ...state,
        questions: chqss,
      };
    case DEL_QUESTION:
      const delqss = deletQuestion(action.payload.question, state.questions);
      saveQuestions(delqss);
      return {
        ...state,
        questions: delqss,
      };
    case DELETE_ANSWERED:
      return {
        ...state,
        questions: deleteAnswered(state.questions),
      };
    case SET_ANSWERED_Q:
      const answeredQ = setAnswered(action.payload.question, state.questions);
      saveQuestions(answeredQ);
      return {
        ...state,
        questions: answeredQ,
      };
    case SET_ALL_QUESTIONS_UNANSWERED:
      const allUnUnswered = setAllUnAnswered(state.questions);
      saveQuestions(allUnUnswered);
      return {
        ...state,
        questions: allUnUnswered,
      };
    case SET_Q_COUNT:
      return {
        ...state,
        roundQuestionsCount: action.payload.newCount,
      };
    case SET_COVER:
      return {
        ...state,
        cardsCover: action.payload.newCover,
      };
      case TOGGLE_BONUS_MODE:
      return {
        ...state,
        noBonusMode: !state.noBonusMode,
      };
    case SET_BG_COLOR:
      return {
        ...state,
        bgColor: action.payload.newColor,
      };
    default:
      return state;
  }
};
