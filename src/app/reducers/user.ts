import * as ActionTypes from '../constants/actionTypes';
import { User } from '../model/user';
import { HasEType } from '../model/other';

export interface UserState {
  user: User;
}

interface UPayload {
  user: User;
}

export interface UserAction extends HasEType {
  payload?: UPayload;
}

const initialState: UserState = {
  user: null,
};

export const userReducer = (state: UserState = initialState, action: UserAction): UserState => {
  switch (action.type) {
    case ActionTypes.SET_USER:
      return { user: action.payload.user };
    default:
      return state;
  }
};
