import { Question } from '../model/question';

export const getNotAnsweredQuestion = (questions: Array<Question>) =>
    questions.find(q => !q.answered);
