import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { createStore } from 'redux';
import { Provider } from 'react-redux';
import { composeWithDevTools } from 'redux-devtools-extension/developmentOnly';

import App from './app';
import reducers from './app/reducers/index';
import ErrorBoundary from './ErrorBoundary';

const store = createStore(reducers, composeWithDevTools());

ReactDOM.render(
    <ErrorBoundary>
      <Provider store={store}>
        <App />
      </Provider>
    </ErrorBoundary>,
    document.getElementById('root') as HTMLElement
);
