const HtmlWebpackPlugin = require('html-webpack-plugin');
const path = require("path");
const webpack = require("webpack");

const defEnv = {
    production: false,
		port: "8088",
		serverIp: '127.0.0.1'
};

module.exports = (env = defEnv) => {
    return {
        mode: env.production && 'production' || 'development',
        entry: {
            app: "./src/index.tsx",
        },
        output: {
            filename: "[name].bundle.js",
            path: path.resolve(__dirname, "dist"),
            publicPath: path.build
        },
        node: {
            fs: "empty"
        },
        plugins: [
            new webpack.EnvironmentPlugin({
                itIsDev: !env.production,
                port: env.port
            }),
            new HtmlWebpackPlugin({
                title: "Своя игра",
                minify:{
                    collapseWhitespace: false
                },
                template: './src/index.html',
                hash:true
            })
        ],
        devServer: {
            contentBase: path.join(__dirname,"dist"),
            compress: true,
            stats: "errors-only",
            port: 9002,
            open: false
        },

        // Enable source maps for debugging webpack's output.
        devtool: !env.production ? "source-map" : false,

        resolve: {
            // Add '.ts' and '.tsx' as resolvable extensions.
            extensions: [ ".webpack.js", ".web.js", ".ts", ".tsx", ".js", ".css"]
        },

        module: {
            rules: [
                    // All files with a '.ts' or '.tsx' extension will be handled by 'awesome-typescript-loader'.
                    { test: /\.tsx?$/, use: "awesome-typescript-loader" },
                    { test: /\.(jpe?g|gif|png|svg|woff|ttf|wav|mp3)$/, use: "file-loader" }
                ],
    
        },
    }
};